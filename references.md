
# References

## General
- https://www.thingiverse.com/thing:2285502

## Node
- http://pm2.keymetrics.io/docs/usage/quick-start/
- https://developer.atlassian.com/blog/2015/11/scripting-with-node/
- https://stackoverflow.com/questions/13167588/what-does-the-g-flag-do-in-the-command-npm-install-g-something

## Homebridge References
- https://github.com/nfarina/homebridge

## Plug-ins

### Other Ecosystems

- https://www.npmjs.com/package/homebridge-lutron-caseta-smockle
- https://www.npmjs.com/package/homebridge-ring
- https://www.npmjs.com/package/homebridge-chamberlain

### Virtualized Accessories

- https://www.npmjs.com/package/homebridge-automation-switches
- https://github.com/grover/homebridge-automation-switches

### Webhooks

- https://www.npmjs.com/package/homebridge-http-webhooks
- https://github.com/benzman81/homebridge-http-webhooks

### Chromecast
- https://www.npmjs.com/package/homebridge-automation-chromecast-play
- https://www.npmjs.com/package/homebridge-automation-chromecast
- https://github.com/pkkid/python-plexapi/issues/63
- https://forum.xda-developers.com/android-tv/chromecast/plex-python-chromecast-t3520651
- https://github.com/balloob/pychromecast

### Network Camera
- https://www.npmjs.com/package/homebridge-camera-ffmpeg
- https://github.com/KhaosT/homebridge-camera-ffmpeg
- https://libraries.io/npm/homebridge-camera-ffmpeg
- https://www.instructables.com/id/Receive-Picture-Notifications-When-People-Approach/
- https://github.com/ccrisan/motioneyeos/
- https://www.youtube.com/watch?v=yOGJfexTSZQ&t=65s

### SMTP sensor
- https://www.npmjs.com/package/homebridge-smtpsensor
- https://github.com/SphtKr/homebridge-smtpsensor

### Plex sensor
- https://medium.com/plexlabs/homebridge-sensors-for-plex-550b572be54d
- https://www.npmjs.com/package/homebridge-plex-sensors
- https://github.com/alexp2ad/homebridge-plex-sensors
- https://python-plexapi.readthedocs.io/en/latest/index.html
- https://www.npmjs.com/package/homebridge-script2
- https://www.npmjs.com/package/homebridge-cmdswitch2

### Presence sensor
- https://www.npmjs.com/package/homebridge-people
- https://www.npmjs.com/package/homebridge-network-sensor

### Heat Controller
- https://www.fibaro.com/en/products/the-heat-controller/

### MQTT sensor
- https://github.com/arachnetech/homebridge-mqttthing
- https://github.com/msiedlarek/winthing

## Home Assistant
- https://www.home-assistant.io/docs/installation/hassbian/installation/
- https://www.home-assistant.io/components/camera.mjpeg/
- https://community.home-assistant.io/t/plex-activity-monitor-using-webhooks-the-easy-way/53233

## Full Page OS
- https://github.com/guysoft/FullPageOS
- https://github.com/guysoft/FullPageOS/issues/247
- https://www.raspberrypi.org/documentation/hardware/display/troubleshooting.md
- https://raspberrypi.stackexchange.com/questions/72118/set-display-resolution-for-official-7-touchscreen-display
- https://scribles.net/setting-up-raspberry-pi-web-kiosk/

## MQTT
- https://mosquitto.org/
- https://www.switchdoc.com/2018/02/tutorial-installing-and-testing-mosquitto-mqtt-on-raspberry-pi/

## ESP8266
- http://docs.micropython.org/en/latest/index.html
- https://randomnerdtutorials.com/micropython-mqtt-esp32-esp8266/
- http://icircuit.net/building-iot-application-using-esp32-micropython-10-steps/2077
- https://obviate.io/2018/02/20/video-tutorial-bme820-environmental-sensor-and-mqtt-under-micropython/
- https://randomnerdtutorials.com/low-power-weather-station-datalogger-using-esp8266-bme280-micropython/
- https://learn.adafruit.com/micropython-basics-load-files-and-run-code/install-ampy
- https://www.reddit.com/r/esp8266/comments/acj23k/log_data_straight_to_google_sheets_using_an/?st=JQI76V1W&sh=b21a9a10
- https://developers.squarespace.com/what-is-json

## GCP
- https://hub.packtpub.com/build-google-cloud-iot-application/
